// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.12.4
// source: sale.proto

package kassa_service

import (
	empty "github.com/golang/protobuf/ptypes/empty"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type ScanProduct struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Barcode string `protobuf:"bytes,1,opt,name=barcode,proto3" json:"barcode,omitempty"`
	SaleId  string `protobuf:"bytes,2,opt,name=sale_id,json=saleId,proto3" json:"sale_id,omitempty"`
}

func (x *ScanProduct) Reset() {
	*x = ScanProduct{}
	if protoimpl.UnsafeEnabled {
		mi := &file_sale_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ScanProduct) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ScanProduct) ProtoMessage() {}

func (x *ScanProduct) ProtoReflect() protoreflect.Message {
	mi := &file_sale_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ScanProduct.ProtoReflect.Descriptor instead.
func (*ScanProduct) Descriptor() ([]byte, []int) {
	return file_sale_proto_rawDescGZIP(), []int{0}
}

func (x *ScanProduct) GetBarcode() string {
	if x != nil {
		return x.Barcode
	}
	return ""
}

func (x *ScanProduct) GetSaleId() string {
	if x != nil {
		return x.SaleId
	}
	return ""
}

type Sale struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id             string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	SaleId         string `protobuf:"bytes,2,opt,name=sale_id,json=saleId,proto3" json:"sale_id,omitempty"`
	SmenaId        string `protobuf:"bytes,3,opt,name=smena_id,json=smenaId,proto3" json:"smena_id,omitempty"`
	BranchId       string `protobuf:"bytes,4,opt,name=branch_id,json=branchId,proto3" json:"branch_id,omitempty"`
	EmployeeId     string `protobuf:"bytes,5,opt,name=employee_id,json=employeeId,proto3" json:"employee_id,omitempty"`
	SalesAddressId string `protobuf:"bytes,6,opt,name=sales_address_id,json=salesAddressId,proto3" json:"sales_address_id,omitempty"`
	Status         string `protobuf:"bytes,7,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *Sale) Reset() {
	*x = Sale{}
	if protoimpl.UnsafeEnabled {
		mi := &file_sale_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Sale) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Sale) ProtoMessage() {}

func (x *Sale) ProtoReflect() protoreflect.Message {
	mi := &file_sale_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Sale.ProtoReflect.Descriptor instead.
func (*Sale) Descriptor() ([]byte, []int) {
	return file_sale_proto_rawDescGZIP(), []int{1}
}

func (x *Sale) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *Sale) GetSaleId() string {
	if x != nil {
		return x.SaleId
	}
	return ""
}

func (x *Sale) GetSmenaId() string {
	if x != nil {
		return x.SmenaId
	}
	return ""
}

func (x *Sale) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *Sale) GetEmployeeId() string {
	if x != nil {
		return x.EmployeeId
	}
	return ""
}

func (x *Sale) GetSalesAddressId() string {
	if x != nil {
		return x.SalesAddressId
	}
	return ""
}

func (x *Sale) GetStatus() string {
	if x != nil {
		return x.Status
	}
	return ""
}

type SaleCreate struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	SaleId         string `protobuf:"bytes,1,opt,name=sale_id,json=saleId,proto3" json:"sale_id,omitempty"`
	SmenaId        string `protobuf:"bytes,2,opt,name=smena_id,json=smenaId,proto3" json:"smena_id,omitempty"`
	BranchId       string `protobuf:"bytes,3,opt,name=branch_id,json=branchId,proto3" json:"branch_id,omitempty"`
	EmployeeId     string `protobuf:"bytes,4,opt,name=employee_id,json=employeeId,proto3" json:"employee_id,omitempty"`
	SalesAddressId string `protobuf:"bytes,5,opt,name=sales_address_id,json=salesAddressId,proto3" json:"sales_address_id,omitempty"`
	Status         string `protobuf:"bytes,6,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *SaleCreate) Reset() {
	*x = SaleCreate{}
	if protoimpl.UnsafeEnabled {
		mi := &file_sale_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SaleCreate) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SaleCreate) ProtoMessage() {}

func (x *SaleCreate) ProtoReflect() protoreflect.Message {
	mi := &file_sale_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SaleCreate.ProtoReflect.Descriptor instead.
func (*SaleCreate) Descriptor() ([]byte, []int) {
	return file_sale_proto_rawDescGZIP(), []int{2}
}

func (x *SaleCreate) GetSaleId() string {
	if x != nil {
		return x.SaleId
	}
	return ""
}

func (x *SaleCreate) GetSmenaId() string {
	if x != nil {
		return x.SmenaId
	}
	return ""
}

func (x *SaleCreate) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *SaleCreate) GetEmployeeId() string {
	if x != nil {
		return x.EmployeeId
	}
	return ""
}

func (x *SaleCreate) GetSalesAddressId() string {
	if x != nil {
		return x.SalesAddressId
	}
	return ""
}

func (x *SaleCreate) GetStatus() string {
	if x != nil {
		return x.Status
	}
	return ""
}

type SaleUpdate struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id             string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	SaleId         string `protobuf:"bytes,2,opt,name=sale_id,json=saleId,proto3" json:"sale_id,omitempty"`
	SmenaId        string `protobuf:"bytes,3,opt,name=smena_id,json=smenaId,proto3" json:"smena_id,omitempty"`
	BranchId       string `protobuf:"bytes,4,opt,name=branch_id,json=branchId,proto3" json:"branch_id,omitempty"`
	EmployeeId     string `protobuf:"bytes,5,opt,name=employee_id,json=employeeId,proto3" json:"employee_id,omitempty"`
	SalesAddressId string `protobuf:"bytes,6,opt,name=sales_address_id,json=salesAddressId,proto3" json:"sales_address_id,omitempty"`
	Status         string `protobuf:"bytes,7,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *SaleUpdate) Reset() {
	*x = SaleUpdate{}
	if protoimpl.UnsafeEnabled {
		mi := &file_sale_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SaleUpdate) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SaleUpdate) ProtoMessage() {}

func (x *SaleUpdate) ProtoReflect() protoreflect.Message {
	mi := &file_sale_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SaleUpdate.ProtoReflect.Descriptor instead.
func (*SaleUpdate) Descriptor() ([]byte, []int) {
	return file_sale_proto_rawDescGZIP(), []int{3}
}

func (x *SaleUpdate) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *SaleUpdate) GetSaleId() string {
	if x != nil {
		return x.SaleId
	}
	return ""
}

func (x *SaleUpdate) GetSmenaId() string {
	if x != nil {
		return x.SmenaId
	}
	return ""
}

func (x *SaleUpdate) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *SaleUpdate) GetEmployeeId() string {
	if x != nil {
		return x.EmployeeId
	}
	return ""
}

func (x *SaleUpdate) GetSalesAddressId() string {
	if x != nil {
		return x.SalesAddressId
	}
	return ""
}

func (x *SaleUpdate) GetStatus() string {
	if x != nil {
		return x.Status
	}
	return ""
}

type SalePrimaryKey struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *SalePrimaryKey) Reset() {
	*x = SalePrimaryKey{}
	if protoimpl.UnsafeEnabled {
		mi := &file_sale_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SalePrimaryKey) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SalePrimaryKey) ProtoMessage() {}

func (x *SalePrimaryKey) ProtoReflect() protoreflect.Message {
	mi := &file_sale_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SalePrimaryKey.ProtoReflect.Descriptor instead.
func (*SalePrimaryKey) Descriptor() ([]byte, []int) {
	return file_sale_proto_rawDescGZIP(), []int{4}
}

func (x *SalePrimaryKey) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type SaleGetListRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Offset           int64  `protobuf:"varint,1,opt,name=offset,proto3" json:"offset,omitempty"`
	Limit            int64  `protobuf:"varint,2,opt,name=limit,proto3" json:"limit,omitempty"`
	SearchBySmenaId  string `protobuf:"bytes,3,opt,name=search_by_smena_id,json=searchBySmenaId,proto3" json:"search_by_smena_id,omitempty"`
	SearchByBranch   string `protobuf:"bytes,4,opt,name=search_by_branch,json=searchByBranch,proto3" json:"search_by_branch,omitempty"`
	SearchByEmployee string `protobuf:"bytes,5,opt,name=search_by_employee,json=searchByEmployee,proto3" json:"search_by_employee,omitempty"`
}

func (x *SaleGetListRequest) Reset() {
	*x = SaleGetListRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_sale_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SaleGetListRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SaleGetListRequest) ProtoMessage() {}

func (x *SaleGetListRequest) ProtoReflect() protoreflect.Message {
	mi := &file_sale_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SaleGetListRequest.ProtoReflect.Descriptor instead.
func (*SaleGetListRequest) Descriptor() ([]byte, []int) {
	return file_sale_proto_rawDescGZIP(), []int{5}
}

func (x *SaleGetListRequest) GetOffset() int64 {
	if x != nil {
		return x.Offset
	}
	return 0
}

func (x *SaleGetListRequest) GetLimit() int64 {
	if x != nil {
		return x.Limit
	}
	return 0
}

func (x *SaleGetListRequest) GetSearchBySmenaId() string {
	if x != nil {
		return x.SearchBySmenaId
	}
	return ""
}

func (x *SaleGetListRequest) GetSearchByBranch() string {
	if x != nil {
		return x.SearchByBranch
	}
	return ""
}

func (x *SaleGetListRequest) GetSearchByEmployee() string {
	if x != nil {
		return x.SearchByEmployee
	}
	return ""
}

type SaleGetListResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Count int64   `protobuf:"varint,1,opt,name=count,proto3" json:"count,omitempty"`
	Sales []*Sale `protobuf:"bytes,2,rep,name=Sales,proto3" json:"Sales,omitempty"`
}

func (x *SaleGetListResponse) Reset() {
	*x = SaleGetListResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_sale_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SaleGetListResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SaleGetListResponse) ProtoMessage() {}

func (x *SaleGetListResponse) ProtoReflect() protoreflect.Message {
	mi := &file_sale_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SaleGetListResponse.ProtoReflect.Descriptor instead.
func (*SaleGetListResponse) Descriptor() ([]byte, []int) {
	return file_sale_proto_rawDescGZIP(), []int{6}
}

func (x *SaleGetListResponse) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

func (x *SaleGetListResponse) GetSales() []*Sale {
	if x != nil {
		return x.Sales
	}
	return nil
}

var File_sale_proto protoreflect.FileDescriptor

var file_sale_proto_rawDesc = []byte{
	0x0a, 0x0a, 0x73, 0x61, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0d, 0x6b, 0x61,
	0x73, 0x73, 0x61, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x1a, 0x1b, 0x67, 0x6f, 0x6f,
	0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x65, 0x6d, 0x70,
	0x74, 0x79, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x40, 0x0a, 0x0b, 0x53, 0x63, 0x61, 0x6e,
	0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x12, 0x18, 0x0a, 0x07, 0x62, 0x61, 0x72, 0x63, 0x6f,
	0x64, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x62, 0x61, 0x72, 0x63, 0x6f, 0x64,
	0x65, 0x12, 0x17, 0x0a, 0x07, 0x73, 0x61, 0x6c, 0x65, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x06, 0x73, 0x61, 0x6c, 0x65, 0x49, 0x64, 0x22, 0xca, 0x01, 0x0a, 0x04, 0x53,
	0x61, 0x6c, 0x65, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x02, 0x69, 0x64, 0x12, 0x17, 0x0a, 0x07, 0x73, 0x61, 0x6c, 0x65, 0x5f, 0x69, 0x64, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x61, 0x6c, 0x65, 0x49, 0x64, 0x12, 0x19, 0x0a, 0x08,
	0x73, 0x6d, 0x65, 0x6e, 0x61, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07,
	0x73, 0x6d, 0x65, 0x6e, 0x61, 0x49, 0x64, 0x12, 0x1b, 0x0a, 0x09, 0x62, 0x72, 0x61, 0x6e, 0x63,
	0x68, 0x5f, 0x69, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x62, 0x72, 0x61, 0x6e,
	0x63, 0x68, 0x49, 0x64, 0x12, 0x1f, 0x0a, 0x0b, 0x65, 0x6d, 0x70, 0x6c, 0x6f, 0x79, 0x65, 0x65,
	0x5f, 0x69, 0x64, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x65, 0x6d, 0x70, 0x6c, 0x6f,
	0x79, 0x65, 0x65, 0x49, 0x64, 0x12, 0x28, 0x0a, 0x10, 0x73, 0x61, 0x6c, 0x65, 0x73, 0x5f, 0x61,
	0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x5f, 0x69, 0x64, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x0e, 0x73, 0x61, 0x6c, 0x65, 0x73, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x49, 0x64, 0x12,
	0x16, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x22, 0xc0, 0x01, 0x0a, 0x0a, 0x53, 0x61, 0x6c, 0x65,
	0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x12, 0x17, 0x0a, 0x07, 0x73, 0x61, 0x6c, 0x65, 0x5f, 0x69,
	0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x61, 0x6c, 0x65, 0x49, 0x64, 0x12,
	0x19, 0x0a, 0x08, 0x73, 0x6d, 0x65, 0x6e, 0x61, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x07, 0x73, 0x6d, 0x65, 0x6e, 0x61, 0x49, 0x64, 0x12, 0x1b, 0x0a, 0x09, 0x62, 0x72,
	0x61, 0x6e, 0x63, 0x68, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x62,
	0x72, 0x61, 0x6e, 0x63, 0x68, 0x49, 0x64, 0x12, 0x1f, 0x0a, 0x0b, 0x65, 0x6d, 0x70, 0x6c, 0x6f,
	0x79, 0x65, 0x65, 0x5f, 0x69, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x65, 0x6d,
	0x70, 0x6c, 0x6f, 0x79, 0x65, 0x65, 0x49, 0x64, 0x12, 0x28, 0x0a, 0x10, 0x73, 0x61, 0x6c, 0x65,
	0x73, 0x5f, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x5f, 0x69, 0x64, 0x18, 0x05, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x0e, 0x73, 0x61, 0x6c, 0x65, 0x73, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73,
	0x49, 0x64, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x06, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x22, 0xd0, 0x01, 0x0a, 0x0a, 0x53,
	0x61, 0x6c, 0x65, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x17, 0x0a, 0x07, 0x73, 0x61, 0x6c,
	0x65, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x61, 0x6c, 0x65,
	0x49, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x73, 0x6d, 0x65, 0x6e, 0x61, 0x5f, 0x69, 0x64, 0x18, 0x03,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x73, 0x6d, 0x65, 0x6e, 0x61, 0x49, 0x64, 0x12, 0x1b, 0x0a,
	0x09, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x69, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x08, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x49, 0x64, 0x12, 0x1f, 0x0a, 0x0b, 0x65, 0x6d,
	0x70, 0x6c, 0x6f, 0x79, 0x65, 0x65, 0x5f, 0x69, 0x64, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x0a, 0x65, 0x6d, 0x70, 0x6c, 0x6f, 0x79, 0x65, 0x65, 0x49, 0x64, 0x12, 0x28, 0x0a, 0x10, 0x73,
	0x61, 0x6c, 0x65, 0x73, 0x5f, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x5f, 0x69, 0x64, 0x18,
	0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0e, 0x73, 0x61, 0x6c, 0x65, 0x73, 0x41, 0x64, 0x64, 0x72,
	0x65, 0x73, 0x73, 0x49, 0x64, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18,
	0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x22, 0x20, 0x0a,
	0x0e, 0x53, 0x61, 0x6c, 0x65, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b, 0x65, 0x79, 0x12,
	0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x22,
	0xc7, 0x01, 0x0a, 0x12, 0x53, 0x61, 0x6c, 0x65, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x12, 0x14,
	0x0a, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x6c,
	0x69, 0x6d, 0x69, 0x74, 0x12, 0x2b, 0x0a, 0x12, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x5f, 0x62,
	0x79, 0x5f, 0x73, 0x6d, 0x65, 0x6e, 0x61, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x0f, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x42, 0x79, 0x53, 0x6d, 0x65, 0x6e, 0x61, 0x49,
	0x64, 0x12, 0x28, 0x0a, 0x10, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x5f, 0x62, 0x79, 0x5f, 0x62,
	0x72, 0x61, 0x6e, 0x63, 0x68, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0e, 0x73, 0x65, 0x61,
	0x72, 0x63, 0x68, 0x42, 0x79, 0x42, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x12, 0x2c, 0x0a, 0x12, 0x73,
	0x65, 0x61, 0x72, 0x63, 0x68, 0x5f, 0x62, 0x79, 0x5f, 0x65, 0x6d, 0x70, 0x6c, 0x6f, 0x79, 0x65,
	0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x10, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x42,
	0x79, 0x45, 0x6d, 0x70, 0x6c, 0x6f, 0x79, 0x65, 0x65, 0x22, 0x56, 0x0a, 0x13, 0x53, 0x61, 0x6c,
	0x65, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x12, 0x14, 0x0a, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52,
	0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x29, 0x0a, 0x05, 0x53, 0x61, 0x6c, 0x65, 0x73, 0x18,
	0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x13, 0x2e, 0x6b, 0x61, 0x73, 0x73, 0x61, 0x5f, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x61, 0x6c, 0x65, 0x52, 0x05, 0x53, 0x61, 0x6c, 0x65,
	0x73, 0x32, 0xdd, 0x02, 0x0a, 0x0b, 0x53, 0x61, 0x6c, 0x65, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x12, 0x3a, 0x0a, 0x06, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x12, 0x19, 0x2e, 0x6b, 0x61,
	0x73, 0x73, 0x61, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x61, 0x6c, 0x65,
	0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x1a, 0x13, 0x2e, 0x6b, 0x61, 0x73, 0x73, 0x61, 0x5f, 0x73,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x61, 0x6c, 0x65, 0x22, 0x00, 0x12, 0x3f, 0x0a,
	0x07, 0x47, 0x65, 0x74, 0x42, 0x79, 0x49, 0x64, 0x12, 0x1d, 0x2e, 0x6b, 0x61, 0x73, 0x73, 0x61,
	0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x61, 0x6c, 0x65, 0x50, 0x72, 0x69,
	0x6d, 0x61, 0x72, 0x79, 0x4b, 0x65, 0x79, 0x1a, 0x13, 0x2e, 0x6b, 0x61, 0x73, 0x73, 0x61, 0x5f,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x61, 0x6c, 0x65, 0x22, 0x00, 0x12, 0x52,
	0x0a, 0x07, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x12, 0x21, 0x2e, 0x6b, 0x61, 0x73, 0x73,
	0x61, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x61, 0x6c, 0x65, 0x47, 0x65,
	0x74, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x22, 0x2e, 0x6b,
	0x61, 0x73, 0x73, 0x61, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x61, 0x6c,
	0x65, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x22, 0x00, 0x12, 0x3a, 0x0a, 0x06, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x12, 0x19, 0x2e, 0x6b,
	0x61, 0x73, 0x73, 0x61, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x61, 0x6c,
	0x65, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x1a, 0x13, 0x2e, 0x6b, 0x61, 0x73, 0x73, 0x61, 0x5f,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x61, 0x6c, 0x65, 0x22, 0x00, 0x12, 0x41,
	0x0a, 0x06, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x12, 0x1d, 0x2e, 0x6b, 0x61, 0x73, 0x73, 0x61,
	0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x61, 0x6c, 0x65, 0x50, 0x72, 0x69,
	0x6d, 0x61, 0x72, 0x79, 0x4b, 0x65, 0x79, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22,
	0x00, 0x42, 0x18, 0x5a, 0x16, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x6b, 0x61,
	0x73, 0x73, 0x61, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x33,
}

var (
	file_sale_proto_rawDescOnce sync.Once
	file_sale_proto_rawDescData = file_sale_proto_rawDesc
)

func file_sale_proto_rawDescGZIP() []byte {
	file_sale_proto_rawDescOnce.Do(func() {
		file_sale_proto_rawDescData = protoimpl.X.CompressGZIP(file_sale_proto_rawDescData)
	})
	return file_sale_proto_rawDescData
}

var file_sale_proto_msgTypes = make([]protoimpl.MessageInfo, 7)
var file_sale_proto_goTypes = []interface{}{
	(*ScanProduct)(nil),         // 0: kassa_service.ScanProduct
	(*Sale)(nil),                // 1: kassa_service.Sale
	(*SaleCreate)(nil),          // 2: kassa_service.SaleCreate
	(*SaleUpdate)(nil),          // 3: kassa_service.SaleUpdate
	(*SalePrimaryKey)(nil),      // 4: kassa_service.SalePrimaryKey
	(*SaleGetListRequest)(nil),  // 5: kassa_service.SaleGetListRequest
	(*SaleGetListResponse)(nil), // 6: kassa_service.SaleGetListResponse
	(*empty.Empty)(nil),         // 7: google.protobuf.Empty
}
var file_sale_proto_depIdxs = []int32{
	1, // 0: kassa_service.SaleGetListResponse.Sales:type_name -> kassa_service.Sale
	2, // 1: kassa_service.SaleService.Create:input_type -> kassa_service.SaleCreate
	4, // 2: kassa_service.SaleService.GetById:input_type -> kassa_service.SalePrimaryKey
	5, // 3: kassa_service.SaleService.GetList:input_type -> kassa_service.SaleGetListRequest
	3, // 4: kassa_service.SaleService.Update:input_type -> kassa_service.SaleUpdate
	4, // 5: kassa_service.SaleService.Delete:input_type -> kassa_service.SalePrimaryKey
	1, // 6: kassa_service.SaleService.Create:output_type -> kassa_service.Sale
	1, // 7: kassa_service.SaleService.GetById:output_type -> kassa_service.Sale
	6, // 8: kassa_service.SaleService.GetList:output_type -> kassa_service.SaleGetListResponse
	1, // 9: kassa_service.SaleService.Update:output_type -> kassa_service.Sale
	7, // 10: kassa_service.SaleService.Delete:output_type -> google.protobuf.Empty
	6, // [6:11] is the sub-list for method output_type
	1, // [1:6] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_sale_proto_init() }
func file_sale_proto_init() {
	if File_sale_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_sale_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ScanProduct); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_sale_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Sale); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_sale_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SaleCreate); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_sale_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SaleUpdate); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_sale_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SalePrimaryKey); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_sale_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SaleGetListRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_sale_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SaleGetListResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_sale_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   7,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_sale_proto_goTypes,
		DependencyIndexes: file_sale_proto_depIdxs,
		MessageInfos:      file_sale_proto_msgTypes,
	}.Build()
	File_sale_proto = out.File
	file_sale_proto_rawDesc = nil
	file_sale_proto_goTypes = nil
	file_sale_proto_depIdxs = nil
}
