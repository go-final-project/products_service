CREATE TABLE product (
    id UUID PRIMARY KEY,
    photo VARCHAR NOT NULL,
    name VARCHAR NOT NULL,
    category_id UUID REFERENCES category("id"),
    brand_id UUID REFERENCES brand("id"),
    barcode VARCHAR NOT NULL,
    price NUMERIC
);