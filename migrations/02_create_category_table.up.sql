CREATE TABLE category (
    id UUID PRIMARY KEY,
    name VARCHAR NOT NULL,
    parent_id UUID REFERENCES category("id"),
    brand_id UUID REFERENCES brand("id")
);