package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"gitlab.com/GoFinalProject/products_service/genproto/product_service"
	"gitlab.com/GoFinalProject/products_service/pkg/helper"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type BrandRepo struct {
	db *pgxpool.Pool
}

func NewBrandRepo(db *pgxpool.Pool) *BrandRepo {
	return &BrandRepo{
		db: db,
	}
}

func (r *BrandRepo) Create(ctx context.Context, req *product_service.BrandCreate) (*product_service.Brand, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO brand(id, name, photo)
		VALUES ($1, $2, $3)
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Name,
		req.Photo,
	)

	if err != nil {
		return nil, err
	}

	return &product_service.Brand{
		Id:    id,
		Name:  req.Name,
		Photo: req.Photo,
	}, nil
}

func (r *BrandRepo) GetByID(ctx context.Context, req *product_service.BrandPrimaryKey) (*product_service.Brand, error) {
	var (
		query string

		id    sql.NullString
		name  string
		photo string
	)

	query = `
		SELECT
			id, name, photo
		FROM brand
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&photo,
	)

	if err != nil {
		return nil, err
	}

	return &product_service.Brand{
		Id:    id.String,
		Name:  name,
		Photo: photo,
	}, nil
}

func (r *BrandRepo) GetList(ctx context.Context, req *product_service.BrandGetListRequest) (*product_service.BrandGetListResponse, error) {

	var (
		resp   = &product_service.BrandGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(), id, name, photo	
		FROM brand
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByName != "" {
		where += ` AND name ILIKE '%' || '` + req.SearchByName + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id    sql.NullString
			name  string
			photo string
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&photo,
		)

		if err != nil {
			return nil, err
		}

		resp.Brands = append(resp.Brands, &product_service.Brand{
			Id:    id.String,
			Name:  name,
			Photo: photo,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *BrandRepo) Update(ctx context.Context, req *product_service.BrandUpdate) (*product_service.Brand, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			brand
		SET
			name = :name,
			photo = :photo
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":    req.Id,
		"name":  req.Name,
		"photo": &req.Photo,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &product_service.Brand{
		Id:    req.Id,
		Name:  req.Name,
		Photo: req.Photo,
	}, nil
}

func (r *BrandRepo) Delete(ctx context.Context, req *product_service.BrandPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM brand WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
