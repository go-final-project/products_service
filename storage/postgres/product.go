package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"gitlab.com/GoFinalProject/products_service/genproto/product_service"
	"gitlab.com/GoFinalProject/products_service/pkg/helper"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type ProductRepo struct {
	db *pgxpool.Pool
}

func NewProductRepo(db *pgxpool.Pool) *ProductRepo {
	return &ProductRepo{
		db: db,
	}
}

func (r *ProductRepo) Create(ctx context.Context, req *product_service.ProductCreate) (*product_service.Product, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO product(id, photo, name, category_id, brand_id, barcode, price)
		VALUES ($1, $2, $3, $4, $5, $6, $7)
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Photo,
		req.Name,
		req.CategoryId,
		req.BrandId,
		req.Barcode,
		req.Price,
	)

	if err != nil {
		return nil, err
	}

	return &product_service.Product{
		Id:         id,
		Photo:      req.Photo,
		Name:       req.Name,
		CategoryId: req.CategoryId,
		BrandId:    req.BrandId,
		Barcode:    req.Barcode,
		Price:      req.Price,
	}, nil
}

func (r *ProductRepo) GetByID(ctx context.Context, req *product_service.ProductPrimaryKey) (*product_service.Product, error) {
	var (
		query string

		id         sql.NullString
		photo      string
		name       string
		categoryId sql.NullString
		brandId    sql.NullString
		barcode    string
		price      int64
	)

	query = `
		SELECT
			id, photo, name, category_id, brand_id, barcode, price
		FROM product
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&photo,
		&name,
		&categoryId,
		&brandId,
		&barcode,
		&price,
	)

	if err != nil {
		return nil, err
	}

	return &product_service.Product{
		Id:         id.String,
		Photo:      photo,
		Name:       name,
		CategoryId: categoryId.String,
		BrandId:    brandId.String,
		Barcode:    barcode,
		Price:      price,
	}, nil
}

func (r *ProductRepo) GetList(ctx context.Context, req *product_service.ProductGetListRequest) (*product_service.ProductGetListResponse, error) {

	var (
		resp   = &product_service.ProductGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(), id, photo, name, category_id, brand_id, barcode, price
		FROM product
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByName != "" {
		where += ` AND name ILIKE '%' || '` + req.SearchByName + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			photo      string
			name       string
			categoryId sql.NullString
			brandId    sql.NullString
			barcode    string
			price      int64
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&photo,
			&name,
			&categoryId,
			&brandId,
			&barcode,
			&price,
		)

		if err != nil {
			return nil, err
		}

		resp.Products = append(resp.Products, &product_service.Product{
			Id:         id.String,
			Photo:      photo,
			Name:       name,
			CategoryId: categoryId.String,
			BrandId:    brandId.String,
			Barcode:    barcode,
			Price:      price,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *ProductRepo) Update(ctx context.Context, req *product_service.ProductUpdate) (*product_service.Product, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			product
		SET
			photo = :photo,
			name = :name,
			category_id = :category_id,
			brand_id = :brand_id,
			barcode = :barcode,
			price = :price
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":          req.Id,
		"photo":       req.Photo,
		"name":        req.Name,
		"category_id": req.CategoryId,
		"brand_id":    req.BrandId,
		"barcode":     req.Barcode,
		"price":       req.Price,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &product_service.Product{
		Id:         req.Id,
		Photo:      req.Photo,
		Name:       req.Name,
		CategoryId: req.CategoryId,
		BrandId:    req.BrandId,
		Barcode:    req.Barcode,
		Price:      req.Price,
	}, nil
}

func (r *ProductRepo) Delete(ctx context.Context, req *product_service.ProductPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM product WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
